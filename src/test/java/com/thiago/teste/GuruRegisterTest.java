package com.thiago.teste;

import com.thiago.core.InvokedMethodListener;
import com.thiago.page.PageRegister;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(InvokedMethodListener.class)
public class GuruRegisterTest {

    @Test
    public void register() {
        final String firstName = "John";
        final String lastName = "Doe";
        final String phone = "+1 234 567890";
        final String userName = "john.doe";
        final String address1 = "5th Ave";
        final String city = "New York";
        final String state = "NY";
        final String postalCode = "10021";
        final String country = "UNITED STATES";
        final String email = "johndoe@example.com";
        final String password = "MyPass123";
        final String confirmPassword = "MyPass123";

        (new PageRegister())
                .openPage(PageRegister.class, "http://demo.guru99.com/test/newtours/register.php")
                .preencherFormulario(
                        firstName, lastName, phone, userName, address1, city, state,
                        postalCode, country, email, password, confirmPassword
                )
                .validarTextoRegistroUsando(firstName, lastName, userName);
    }

}
