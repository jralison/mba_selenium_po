package com.thiago.teste;

import com.thiago.core.InvokedMethodListener;
import com.thiago.page.PageDeleteCustomer;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(InvokedMethodListener.class)
public class GuruDeleteCustomerTest {

    @Test
    public void delete() {
        (new PageDeleteCustomer())
                .openPage(PageDeleteCustomer.class, "http://demo.guru99.com/test/delete_customer.php")
                .enviarIdParaExclusao("ID123456")
                .validarConfirmacaoExclusao();
    }

}
