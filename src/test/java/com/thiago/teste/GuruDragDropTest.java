package com.thiago.teste;

import com.thiago.core.InvokedMethodListener;
import com.thiago.page.PageGuruDragDrop;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(InvokedMethodListener.class)
public class GuruDragDropTest {

    @Test
    public void testDragDrop() {
        new PageGuruDragDrop()
                .openPage(PageGuruDragDrop.class, "http://demo.guru99.com/test/drag_drop.html")
                .moverCelulas()
                .validarDroppedState();
    }
}
