package com.thiago.teste;

import com.thiago.core.InvokedMethodListener;
import com.thiago.page.PageGuruLogin;
import com.thiago.page.PageGuruSuccess;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(InvokedMethodListener.class)
public class GuruLoginTest {

    @Test
    public void login() {
        final PageGuruLogin pageLogin = new PageGuruLogin();
        pageLogin.openPage(PageGuruLogin.class, "http://demo.guru99.com/test/login.html");
        pageLogin.entrarUsando("email@example.com", "senha123");

        final PageGuruSuccess pageSuccess = new PageGuruSuccess();
        pageSuccess.validarPaginaSucesso();
        pageSuccess.validarMensagemSucesso();
    }

}
