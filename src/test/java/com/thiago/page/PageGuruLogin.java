package com.thiago.page;

import com.thiago.core.CorePage;
import com.thiago.driver.TLDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageGuruLogin extends CorePage<PageGuruLogin> {

    public PageGuruLogin() {
        this.driver = TLDriverFactory.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(id = "login_form")
    private WebElement formLogin;

    @FindBy(id = "email")
    private WebElement inputEmail;

    @FindBy(id = "passwd")
    private WebElement inputPassword;

    @FindBy(id = "SubmitLogin")
    private WebElement btnLogin;

    public void entrarUsando(String email, String senha) {
        aguardarElementoVisivel(formLogin);

        preencherCampo(inputEmail, email);
        preencherCampo(inputPassword, senha);
        click(btnLogin);
    }

}
