package com.thiago.page;

import com.thiago.core.CorePage;
import com.thiago.driver.TLDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class PageGuruDragDrop extends CorePage<PageGuruDragDrop> {

    @FindBy(id = "bank")
    private WebElement dropDebitAccount;

    @FindBy(id = "loan")
    private WebElement dropCreditAccount;

    @FindBy(id = "amt7")
    private WebElement dropDebitAmount;

    @FindBy(id = "amt8")
    private WebElement dropCreditAmount;

    @FindBy(id = "fourth")
    private WebElement dragCellFourth;

    @FindBy(id = "credit2")
    private WebElement dragCellBank;

    @FindBy(id = "credit1")
    private WebElement dragCellSales;

    @FindBy(id = "equal")
    private WebElement spanPerfect;

    public PageGuruDragDrop() {
        this.driver = TLDriverFactory.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    public PageGuruDragDrop moverCelulas() {
        aguardarElementoVisivel(dragCellSales);
        aguardarElementoVisivel(dropDebitAmount);

        (new Actions(driver))
                .dragAndDrop(dragCellBank, dropDebitAccount)
                .dragAndDrop(dragCellSales, dropCreditAccount)
                .dragAndDrop(dragCellFourth, dropCreditAmount)
                .dragAndDrop(dragCellFourth, dropDebitAmount)
            .build().perform();

        return this;
    }

    public void validarDroppedState() {
//        aguardarElementoVisivel(spanPerfect);
        Assert.assertTrue(spanPerfect.isDisplayed());
        Assert.assertEquals(spanPerfect.getText(), "Perfect!");
    }

}
