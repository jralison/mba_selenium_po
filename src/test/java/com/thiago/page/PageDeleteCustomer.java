package com.thiago.page;

import com.thiago.core.CorePage;
import com.thiago.driver.TLDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.logging.Logger;

public class PageDeleteCustomer extends CorePage<PageDeleteCustomer> {

    @FindBy(name = "cusid")
    private WebElement inputCustomerId;

    @FindBy(xpath = "/html/body/form/table/tbody/tr[3]/td[2]/input[1]")
    private WebElement btnSubmit;

    public PageDeleteCustomer() {
        this.driver = TLDriverFactory.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    public PageDeleteCustomer enviarIdParaExclusao(String customerId) {
        aguardarElementoVisivel(inputCustomerId);
        preencherCampo(inputCustomerId, customerId);

        aguardarElementoClicado(btnSubmit);
        btnSubmit.click();

        final String textAlert = getAlert();
        Logger.getAnonymousLogger().info("#### PRIMEIRO ALERTA ####: " + textAlert);
        Assert.assertEquals("Do you really want to delete this Customer?", textAlert);

        acceptAlert();

        return this;
    }

    public void validarConfirmacaoExclusao() {
        final String textAlert = getAlert();
        Logger.getAnonymousLogger().info("#### SEGUNDO ALERTA ####: " + textAlert);
        Assert.assertEquals("Customer Successfully Delete!", textAlert);
        acceptAlert();
    }

}
