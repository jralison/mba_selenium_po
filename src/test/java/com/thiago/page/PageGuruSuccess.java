package com.thiago.page;

import com.thiago.core.CorePage;
import com.thiago.driver.TLDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class PageGuruSuccess extends CorePage<PageGuruSuccess> {

    public PageGuruSuccess() {
        this.driver = TLDriverFactory.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(css = ".error-box h3")
    private WebElement h3Text;

    public void validarPaginaSucesso() {
        aguardarElementoVisivel(h3Text);
        Assert.assertEquals(driver.getCurrentUrl(), "http://demo.guru99.com/test/success.html");
    }

    public void validarMensagemSucesso() {
        aguardarElementoVisivel(h3Text);
        Assert.assertEquals(h3Text.getText(), "Successfully Logged in...");
    }

}
