package com.thiago.page;

import com.thiago.core.CorePage;
import com.thiago.driver.TLDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.logging.Logger;

public class PageRegisterSuccess extends CorePage<PageRegisterSuccess> {

    @FindBy(xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[3]")
    private WebElement rowMessage;

    public PageRegisterSuccess() {
        this.driver = TLDriverFactory.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    public void validarTextoRegistroUsando(String nome, String sobrenome, String usuario) {
        aguardarElementoVisivel(rowMessage);

        final String textMessage = rowMessage.getText();
        Logger.getAnonymousLogger().info("Texto obtido: " + textMessage);

        Logger.getAnonymousLogger().info("Texto saudação esperado: Dear " + nome + " " + sobrenome);
        Assert.assertTrue(textMessage.contains("Dear " + nome + " " + sobrenome));

        Logger.getAnonymousLogger().info("Texto encerramento esperado: Your user name is " + usuario);
        Assert.assertTrue(textMessage.contains("Your user name is " + usuario));
    }
}
