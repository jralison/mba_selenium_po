package com.thiago.page;

import com.thiago.core.CorePage;
import com.thiago.driver.TLDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageRegister extends CorePage<PageRegister> {

    @FindBy(xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form")
    private WebElement form;

    @FindBy(name = "submit")
    private WebElement btnSubmit;

    @FindBy(name = "firstName")
    private WebElement inputFirstName;

    @FindBy(name = "lastName")
    private WebElement inputLastName;

    @FindBy(name = "phone")
    private WebElement inputPhone;

    @FindBy(name = "email") // o campo de usuário está nomeado como email na interface
    private WebElement inputUserName;

    @FindBy(name = "address1")
    private WebElement inputAddress1;

    @FindBy(name = "city")
    private WebElement inputCity;

    @FindBy(name = "state")
    private WebElement inputState;

    @FindBy(name = "postalCode")
    private WebElement inputPostalCode;

    @FindBy(name = "country")
    private WebElement inputCountry;

    @FindBy(name = "userName") // o campo de email está nomeado como userName na interface
    private WebElement inputEmail;

    @FindBy(name = "password")
    private WebElement inputPassword;

    @FindBy(name = "confirmPassword")
    private WebElement inputConfirmPassword;

    public PageRegister() {
        this.driver = TLDriverFactory.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    public PageRegisterSuccess preencherFormulario(String firstName, String lastName, String phone,
                                                   String userName, String address1, String city, String state,
                                                   String postalCode, String country, String email,
                                                   String password, String confirmPassword) {
        aguardarElementoVisivel(form);

        preencherCampo(inputFirstName, firstName);
        preencherCampo(inputLastName, lastName);
        preencherCampo(inputPhone, phone);
        preencherCampo(inputUserName, userName);
        preencherCampo(inputAddress1, address1);
        preencherCampo(inputCity, city);
        preencherCampo(inputState, state);
        preencherCampo(inputPostalCode, postalCode);
        selectElementByVisibleValue(inputCountry, country);
        preencherCampo(inputEmail, email);
        preencherCampo(inputPassword, password);
        preencherCampo(inputConfirmPassword, confirmPassword);

        aguardarElementoClicado(btnSubmit);
        btnSubmit.click();

        return new PageRegisterSuccess();
    }

}
